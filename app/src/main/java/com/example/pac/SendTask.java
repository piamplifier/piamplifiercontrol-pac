package com.example.pac;

import android.os.AsyncTask;
import android.util.Log;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Notification;

public class SendTask extends AsyncTask<JSONRPC2Notification, Void, Void> {
    SocketBridge mSocketBridge;

    SendTask(SocketBridge sockBridge) {
        mSocketBridge = sockBridge;
    }

    @Override
    protected Void doInBackground(JSONRPC2Notification... requests) {
        for(JSONRPC2Notification req:requests) {
            Log.v("SendTask","Sending command: " + req.toJSONString());
            mSocketBridge.call_method(req);
        }
        return null;
    }
}
