package com.example.pac;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Notification;
import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.SocketFactory;

public class MainActivity extends Activity {
    private SocketFactory mSocketFactory = null;
    private SocketBridge mSocketBridge = null;
    private SwitchMaterial mAmpOnOffButton;
    private SeekBar mInputSelector;
    private SeekBar mVolumeControl;
    private SeekBar mBassControl;
    private SeekBar mMidControl;
    private SeekBar mTrebleControl;
    private TextView mCurrentInput;
    private TextView mCurrentVolume;
    private final String host = "192.168.50.45";
    private final int port = 4025;
    final String[] inputs = {"Aux1", "Aux2", "Aux3", "Aux4", "PiAudio"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Get all the widget objects. */
        mAmpOnOffButton = findViewById(R.id.amp_on_off);
        mInputSelector = findViewById(R.id.input_selector);
        mCurrentInput = findViewById(R.id.current_input);
        mVolumeControl = findViewById(R.id.volume);
        mCurrentVolume = findViewById(R.id.current_volume);
        mBassControl = findViewById(R.id.bass_gain);
        mMidControl = findViewById(R.id.mid_gain);
        mTrebleControl = findViewById(R.id.treble_gain);

        NetworkRequest.Builder reqBuilder = new NetworkRequest.Builder();
        reqBuilder.addTransportType(NetworkCapabilities.TRANSPORT_WIFI);
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        cm.requestNetwork(reqBuilder.build(), new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                mSocketFactory = network.getSocketFactory();
                mSocketBridge = new SocketBridge(host, port, mSocketFactory);
            }
        });

        mAmpOnOffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Object> params = new Vector<Object>();
                if(mAmpOnOffButton.isChecked()) {
                    Log.v("MainActivity","Unmute amplifier");
                    params.add(false);
                } else {
                    Log.v("MainActivity","Mute amplifier");
                    params.add(true);
                }
                JSONRPC2Notification notification = new JSONRPC2Notification("mute_amplifier", params);
                new SendTask(mSocketBridge).execute(notification);
            }
        });

        mInputSelector.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mCurrentInput.setText(String.format("Selected input: %s", inputs[progress]));
                List<Object> params = new Vector<Object>();
                params.add(inputs[progress]);
                JSONRPC2Notification notification = new JSONRPC2Notification("set_input", params);
                new SendTask(mSocketBridge).execute(notification);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mVolumeControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mCurrentVolume.setText(String.format("Volume %d", progress));
                List<Object> params = new Vector<Object>();
                params.add(progress);
                JSONRPC2Notification notification = new JSONRPC2Notification("set_volume", params);
                new SendTask(mSocketBridge).execute(notification);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mBassControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                List<Object> params = new Vector<Object>();
                params.add(progress-14);
                JSONRPC2Notification notification = new JSONRPC2Notification("set_bass_gain", params);
                new SendTask(mSocketBridge).execute(notification);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mMidControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                List<Object> params = new Vector<Object>();
                params.add(progress-14);
                JSONRPC2Notification notification = new JSONRPC2Notification("set_mid_gain", params);
                new SendTask(mSocketBridge).execute(notification);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mTrebleControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                List<Object> params = new Vector<Object>();
                params.add(progress-14);
                JSONRPC2Notification notification = new JSONRPC2Notification("set_treble_gain", params);
                new SendTask(mSocketBridge).execute(notification);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        String[] commands = {"amplifier_muted", "get_input", "get_volume",
                "get_bass_gain", "get_mid_gain", "get_treble_gain"};
        executor.execute(() -> {
            HashMap<String, Object> responses = new HashMap<String, Object>();
            int id = 180;
            for(String command:commands) {
                JSONRPC2Request req = new JSONRPC2Request(command, id++);
                Log.v("UpdateUI","Sending command: " + req.toJSONString());
                try {
                    JSONRPC2Response json_resp = JSONRPC2Response.parse(mSocketBridge.get_data(req));
                    Log.v("UpdateUI", "Received response: " + json_resp.toJSONString());
                    responses.put(command, json_resp.getResult());
                } catch (JSONRPC2ParseException e) {
                    throw new RuntimeException(e);
                }
            }
            handler.post(() -> {
                for(HashMap.Entry<String, Object> resp: responses.entrySet()) {
                    switch(resp.getKey()){
                        case "amplifier_muted":
                            mAmpOnOffButton.setActivated((Boolean) resp.getValue());
                            break;
                        case "get_input":
                            List<String> list = Arrays.asList(inputs);
                            int input_number = list.indexOf((String) resp.getValue());
                            mInputSelector.setProgress(input_number);
                            break;
                        case "get_volume":
                            mVolumeControl.setProgress(((Long) resp.getValue()).intValue());
                            break;
                        case "get_bass_gain":
                            int bass_gain = ((Long) resp.getValue()).intValue();
                            bass_gain += 14;
                            mBassControl.setProgress(bass_gain);
                            break;
                        case "get_mid_gain":
                            int mid_gain = ((Long) resp.getValue()).intValue();
                            mid_gain += 14;
                            mMidControl.setProgress(mid_gain);
                            break;
                        case "get_treble_gain":
                            int treble_gain = ((Long) resp.getValue()).intValue();
                            treble_gain += 14;
                            mTrebleControl.setProgress(treble_gain);
                            break;
                    }
                }
            });
        });
    }
}
