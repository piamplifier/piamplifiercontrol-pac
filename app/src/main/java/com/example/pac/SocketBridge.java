package com.example.pac;

import android.util.Log;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Notification;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import javax.net.SocketFactory;

public class SocketBridge {
    private SocketFactory mSocketFactory;
    private String mHost;
    private int mPort;
    private String mTag = "SocketBridge";

    SocketBridge(String host, int port, SocketFactory sockFact) {
        mPort = port;
        mHost = host;
        mSocketFactory = sockFact;
    }

    void call_method(JSONRPC2Notification reqOut) {
        Socket socket = null;
        OutputStream outStream = null;

        // Serialise the request to a JSON-encoded string with null termination.
        String jsonString = reqOut.toString() + '\n';

        try {
            //SocketAddress sockAddr = new InetSocketAddress(host, port);
            //mSocket = new Socket();
            socket = mSocketFactory.createSocket(mHost, mPort);
            Log.v("SocketBridge","Created socket.");
            if(socket.isConnected()) {
                Log.v("SocketBridge","Sending data.");
                outStream = socket.getOutputStream();
                outStream.write(jsonString.getBytes());
                outStream.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("SocketBridge", "doInBackground: IOException");
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("SocketBridge", "doInBackground: Exception");
        } finally {
            try {
                socket.close();
                outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.i("SocketBridge", "doInBackground: IOException");
            } catch (Exception e) {
                e.printStackTrace();
                Log.i("SocketBridge", "doInBackground: Exception");
            }
        }
    }

    String get_data(JSONRPC2Request reqOut) {
        Socket socket = null;
        String response = null;
        InputStream inStream = null;
        OutputStream outStream = null;

        // Serialise the request to a JSON-encoded string with null termination.
        String jsonString = reqOut.toString() + '\n';

        try {
            //SocketAddress sockAddr = new InetSocketAddress(host, port);
            //mSocket = new Socket();
            socket = mSocketFactory.createSocket(mHost, mPort);
            socket.setSoTimeout(200);
            if(socket.isConnected()) {
                inStream = socket.getInputStream();
                outStream = socket.getOutputStream();

                outStream.write(jsonString.getBytes());
                outStream.flush();

                byte[] inBuffer = new byte[1024];
                int bytesRead = inStream.read(inBuffer, 0, 1024);
                int lastBytesRead = bytesRead;
                while(lastBytesRead != -1) {
                    /* Break when the null termination is found. */
                    if(inBuffer[bytesRead] == 0x00){
                        break;
                    }
                    lastBytesRead = inStream.read(inBuffer, bytesRead, 1024 - bytesRead);
                    bytesRead += lastBytesRead;
                }
                response = new String(inBuffer, StandardCharsets.UTF_8);
                response = response.replace("\0", "");
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("SocketBridge", "doInBackground: IOException");
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("SocketBridge", "doInBackground: Exception");
        } finally {
            try {
                socket.close();
                inStream.close();
                outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.i("SocketBridge", "doInBackground: IOException");
            } catch (Exception e) {
                e.printStackTrace();
                Log.i("SocketBridge", "doInBackground: Exception");
            }
        }
        return response;
    }
}
